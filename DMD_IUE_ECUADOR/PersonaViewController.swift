//
//  PersonaViewController.swift
//  DMD_IUE_ECUADOR
//
//  Created by Catarina Silva on 25/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonaViewController: UIViewController {

    var persona : Persona?
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var primerNombre: UITextField!
    
    @IBOutlet weak var apellido: UITextField!
    
    @IBOutlet weak var nacionalidad: UITextField!
    
    @IBAction func setButtonPressed(_ sender: AnyObject) {
        if persona == nil {
            persona = Persona(firstName: primerNombre.text!, lastName: apellido.text!, nationality: nacionalidad.text!)
            //appDelegate.personas.append(p)
            //save in the array on the app memory
            ArchivingRepository.repository.personas.append(persona!)
            //save in the file in the app bundle (persistent)
            ArchivingRepository.repository.savePersonas()
            
        }
        else {
            persona!.firstName = primerNombre.text
            persona!.lastName = apellido.text
            persona!.nationality = nacionalidad.text!
            
        }
        
        textLabel.text = persona!.fullName()
        
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        primerNombre.text = persona?.firstName
        apellido.text = persona?.lastName
        nacionalidad.text = persona?.nationality
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
