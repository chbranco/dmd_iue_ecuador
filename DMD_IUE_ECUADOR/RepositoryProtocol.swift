//
//  RepositoryProtocol.swift
//  DMD_IUE_ECUADOR
//
//  Created by Catarina Silva on 27/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func savePersonas()
    func loadPersonas()
}
