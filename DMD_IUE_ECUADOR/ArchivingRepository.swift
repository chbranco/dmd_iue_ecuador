//
//  ArchivingRepository.swift
//  DMD_IUE_ECUADOR
//
//  Created by Catarina Silva on 27/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ArchivingRepository:RepositoryProtocol {
    
    static let repository = ArchivingRepository() //shared instance; singleton
    fileprivate init() {} //This prevents others from using the default '()' initializer for this class.

    var personas = [Persona]()
    
    var documentsPath:URL = URL(fileURLWithPath: "")
    var filePath: URL = URL(fileURLWithPath: "")
    
    func savePersonas() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("Personas.data", isDirectory: false)
        let path = filePath.path
        
        print("######################save people \(path)")
        
        if NSKeyedArchiver.archiveRootObject(personas, toFile: path) {
            print("Success saving")
        } else {
            print("Error saving")
        }
    }
    func loadPersonas() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("Personas.data", isDirectory: false)
        let path = filePath.path
        
        print("######################load people \(path)")


        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Persona] {
            personas = newData
        }
    }
}


