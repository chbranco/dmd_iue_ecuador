//
//  Persona.swift
//  DMD_IUE_ECUADOR
//
//  Created by Catarina Silva on 25/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class Persona: NSObject, NSCoding {
    var firstName:String?
    var lastName:String?
    //var nationality:String = "Ecuatoriano"
    var nationality:String
    
    override init() {
        nationality = "Ecuatoriano"
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "nombre")
        aCoder.encode(lastName, forKey: "apellido")
        aCoder.encode(nationality, forKey: "nacionalidad")
    }
    
    required init?(coder aDecoder: NSCoder) {
        firstName = aDecoder.decodeObject(forKey: "nombre") as? String
        lastName = aDecoder.decodeObject(forKey: "apellido") as? String
        if let nat = aDecoder.decodeObject(forKey: "nacionalidad") as? String {
          nationality = nat
        } else {
            nationality = "Ecuatoriano"
        }
        //nationality = aDecoder.decodeObject(forKey: "nacionalidad") as! String
        
    }
    
    init(firstName:String, lastName:String, nationality: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName () -> String {
        //if sure that vars have values:
        //return firstName! + " " + lastName!
        
        var name = ""
        
        if let fn = firstName {
            name = fn
            if let ln = lastName {
                name = name + " " + ln
            }
        }
        return name
    }
}





